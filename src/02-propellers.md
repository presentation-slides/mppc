## Propellers

* A propeller generates a force by rotating propeller blade(s)
* The propeller blades generate a lifting force *(thrust)* by pitching at an Angle of Attack

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/mppc/propeller-blade-aoa-1.png">
    <img src="https://presentation-slides-media.gitlab.io/mppc/propeller-blade-aoa-1.png" alt="Propeller Blade AoA 1" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Propellers

* As an aircraft travels forward, the resultant relative air flow on the propeller changes angle
* This reduces the Angle of Attack of the propeller blades, producing less thrust
* By allowing a change in the pitch of the propeller, we achieve more *Thrust Available* at higher airspeeds

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/mppc/propeller-blade-aoa-2.png">
    <img src="https://presentation-slides-media.gitlab.io/mppc/propeller-blade-aoa-2.png" alt="Propeller Blade AoA 2" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

## Propeller Forces { data-transition="convex-in slide-out" }

* A propeller is essentially a rotating wing
* <strong>Centrifugal</strong> Force &mdash; the force <strong>on</strong> the blades acting to pull them <strong>out</strong> of the hub, opposes bending forces
* <strong>Torque Bending Force</strong> tends to bend the propeller blades in the direction opposite that of rotation
* <strong>Thrust Bending Force</strong> thrust load that tends to bend propeller blades forward as the aircraft is pulled through the air
* <strong>Aerodynamic</strong> twisting moment (i.e. coarse pitch)
* <strong>Centrifugal</strong> twisting moment (i.e. fine pitch)

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/propeller-forces.png" alt="Propeller Forces" height="200px"/>
</p>


---

