## The Basics of a Governor { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/csu.png" alt="CSU" height="380px"/>
</p>

* The propeller pitch is changed hydraulically, using the same oil the engine uses
* A governor sends oil to and from the propeller hub in order to change the blade pitch

---

