## Fine and Coarse Pitch { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/propeller-fine-coarse-pitch.jpg" alt="Propeller fine/coarse pitch" height="300px"/>
</p>

* When the <span class="prop-lever">propeller lever</span> is fully forward, the propeller is in fine pitch
* Fine pitch is used for takeoff, landing, go-around and ground run-ups

---

