## Manifold Pressure & RPM { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/manifold-pressure-gauge-tachometer.png" alt="Manifold Pressure Gauge and Tachometer" height="300px"/>
</p>

* With the engine stopped, the MP gauge will read ambient atmospheric pressure
* On startup, with the propeller in fine pitch, the RPM & MP will change with the <span class="throttle">throttle</span> &mdash; just like a fixed pitch propeller
* If the RPM is increased with the <span class="prop-lever">propeller lever</span>, MP will rise & vice versa
* Carburettor & Induction Icing
  * is indicated by an unexplained MP drop and loss of performance
  * **The RPM indication will remain constant**

---

