## Manifold Pressure & RPM { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/manifold-pressure-gauge.jpg" alt="Manifold Pressure Gauge" height="300px"/>
</p>

* Because the governor maintains a given RPM, the tachometer is no longer a reliable indication of the engine power output
* The manifold pressure gauge (MP) is now our measure of power
* Manifold pressure is generally measured in inches of mercury (inHg) from the engine inlet manifold

---

