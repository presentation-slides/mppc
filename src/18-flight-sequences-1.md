## Flight Sequences { data-transition="convex-in slide-out" }

* Pre-flight inspection
  * Inspect propeller hub
  * Inspect propeller blades for any play in hub
* Engine run-up
  * Propeller cycle (x2), expecting RPM drop 200-300RPM
  * Observe the engine speed at which the CSU begins governing the propeller blade pitch angle
* Climb out
  1. Set <span class="throttle">manifold pressure</span>
  2. Set <span class="prop-lever">RPM</span>
* Level out for cruise *(departure)*
  1. Reduce <span class="throttle">manifold pressure</span>
  2. Reduce <span class="prop-lever">RPM</span>
  3. *the manifold pressure will increase slightly after setting RPM*
* Observe **constant RPM** with differing airspeeds
* Engine failure procedures
  * Observe glide performance with fine/coarse propeller pitch

---

