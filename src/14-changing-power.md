## Changing Power { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/changing-power.jpg" alt="Changing Power" height="380px"/>
</p>

* When decreasing power, it is important to <span class="throttle">decrease MP</span> before <span class="prop-lever">decreasing RPM</span>
* When increasing power, it is important <span class="prop-lever">increase RPM</span> before <span class="throttle">increasing MP</span>
* Just like changing down a gear to go up a hill

---

