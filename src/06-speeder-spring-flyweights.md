## Speeder Spring & Flyweights { data-transition="convex-in slide-out" }

* <span style="font-size: large">The <span class="prop-lever">propeller lever</span> sets the tension on the speeder spring, which sets the position of the flyweights</span>
* <span style="font-size: large">The flyweights then set the position of the pilot valve</span>
* <span style="font-size: large">The pilot valve can be fully open, partially restricted or fully closed with regard to oil flow to the propeller hub</span>
* <span style="font-size: large">If the RPM on the propeller would otherwise increase with an increasing airspeed, the flyweights also move outwards (because the flyweights are directly connected to the engine crankshaft) due to centrifugal force</span>
* <span style="font-size: large">This opens the pilot valve and allows more oil pressure directed to the propeller hub, moving the propeller blades to a coarser position</span>
* <span style="font-size: large">The opposite is also true</span>

---

