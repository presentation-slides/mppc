---
title: Manual Propeller Pitch Control
subtitle: Principles of Operation
---

<style>

.fuel-mixture {
    color: white;
    background: #FF5555;
    border-radius: 5px;
    border: 2px solid red;
    padding: 0px 1px 0px 1px;
    display: inline-block;
    margin: 0.5px 0px;
}

.prop-lever {
    color: white;
    background: #0A60FF;
    border-radius: 5px;
    border: 2px solid blue;
    padding: 0px 1px 0px 1px;
    display: inline-block;
    margin: 0.5px 0px;
}

.throttle {
    color: white;
    background: #393939;
    border-radius: 5px;
    border: 2px solid black;
    padding: 0px 1px 0px 1px;
    display: inline-block;
    margin: 0.5px 0px;
}

</style>
