## Quiz on Objectives { data-transition="convex-in slide-out" }

* What are some of the forces acting on a propeller?
* By what mechanism does a propeller governer change propeller pitch?
* How are the flyweights spun and what do they in turn do within the governor?
* What happens to propeller pitch with a loss of oil pressure?

---

