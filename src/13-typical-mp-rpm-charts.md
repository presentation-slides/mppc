## Cessna 182P Cruise Performance { data-transition="convex-in slide-out" }

<span style="font-size: small">
  <ul>
    <li>Pressure Altitude 4000ft</li>
    <li>2950 lb</li>
    <li>Cowl flaps closed</li>
    <li>Fuel mixture leaned</li>
  </ul>
<span>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/c182p-cruise-performance-4000.png" alt="Cessna 182P Cruise Performance" height="480px"/>
</p>

---

