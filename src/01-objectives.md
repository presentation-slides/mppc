## Objectives { data-transition="convex-in slide-out" }

* Achieve an understanding of the Constant Speed System
* Achieve an understanding of Propellers and Propeller Forces
* Obtain an ability to safely and efficiently operate a Constant Speed Unit (CSU)

---

