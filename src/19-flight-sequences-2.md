## Flight Sequences { data-transition="convex-in slide-out" }

* Enter a climb
  1. <span class="fuel-mixture">Fuel mixture</span> full
  2. Set <span class="prop-lever">RPM</span>
  3. Set <span class="throttle">manifold pressure</span>
* Top of climb
  1. Set <span class="throttle">manifold pressure</span>
  2. Set <span class="prop-lever">RPM</span>
  3. <span class="fuel-mixture">Fuel mixture</span> lean
* Top of descent
  1. <span class="fuel-mixture">Fuel mixture</span> slightly richer
  2. *the manifold pressure may increase due to RAM air and/or increased air density*
  3. Adjust <span class="throttle">manifold pressure</span> accordingly
* Go-around
  * "Mixture UP"
  * "Pitch UP"
  * "Power UP"
  * +RoC&hellip; "Gear UP"
  * "Flaps UP"
