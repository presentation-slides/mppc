## Pilot Valve { data-transition="convex-in slide-out" }

* The governor pilot valve is moved up and down by the flyweights, allowing oil to flow into, or out of, the propeller hub
* Again, the amount of oil is controlled by the propellor governor

---

## Explanation of a Propeller Governor { data-transition="convex-in slide-out" }

<iframe width="560" height="315" src="https://www.youtube.com/embed/kCSKhDL0bXM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

