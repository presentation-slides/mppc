## Airmanship/TEM/HF/LOC { data-transition="convex-in slide-out" }

<section>
  <table>
      <thead>
        <tr>
            <th>Threat</th>
            <th>Error</th>
            <th>Management</th>
            <th>Mitigation</th>
        </tr>
      </thead>
      <tbody>
        <tr style="font-size: x-large">
          <td>Mismanagement of Throttle and/or Propeller RPM</td>
          <td>RPM Redline condition</td>
          <td>Monitor engine indication parameters</td>
          <td>Include MP gauge and Tachometer in instrument scan</td>
        </tr>
      </tbody>
    </table>
</section>

---

