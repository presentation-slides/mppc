## Fine and Coarse Pitch { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/propeller-fine-coarse-pitch2.jpg" alt="Propeller fine/coarse pitch" height="300px"/>
</p>

* When the <span class="prop-lever">propeller lever</span> is pulled backward, the pitch of the blades become coarser
* Think of it like a car
  * 80kph in second gear may be somewhere around 6000 RPM
  * However, 80kph in sixth gear is around 2000 RPM
  * When we move the <span class="prop-lever">propeller lever</span> backward, we are "changing to a higher gear"

---

