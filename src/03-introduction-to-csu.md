## Introduction to the CSU { data-transition="convex-in slide-out" }

* A Constant Speed Unit, or Manual Propeller Pitch Control, maintains the propeller RPM at a speed selected by the pilot
* The blade angle of the propeller self-adjusts to maintain a given RPM independent of the <span class="throttle">throttle lever</span> or manifold pressure

---

