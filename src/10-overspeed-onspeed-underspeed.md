## Overspeed, On Speed & Underspeed { data-transition="convex-in slide-out" }

* When the propeller is in an <strong>On Speed</strong> position, no oil flows in or out of the hub &mdash; maintaining a constant RPM
* If the RPM would otherwise decrease, to maintain RPM, the spring force becomes greater than centrifugal forces, forcing the pilot valve down. This is known as an <strong>Underspeed</strong> condition and returns the blades to a finer pitch.
* If the RPM would otherwise increase, to maintain RPM, the centrifugal forces become greater than the speeder spring force. The pilot valve lifts, allowing oil to flow into the propeller hub &mdash; moving the blades towards a coarser pitch &mdash; this is known as an <strong>Overspeed</strong> condition
* If the propeller does overspeed, <strong>reduce power/airspeed</strong> and if required, use <span class="prop-lever">propeller lever</span> to control RPM
* Remember, blade angle and RPM is a function of TAS and power setting

---

