## Propeller Hub { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/csu-governor.jpg" alt="The Governor" height="300px"/>
</p>

* <span style="font-size: large">Oil flow is controlled by the propeller governor, which is set by the <span class="prop-lever">propeller lever</span> to maintain a given RPM</span>
* <span style="font-size: large">The <span class="prop-lever">propeller lever</span> changes the compression of the speeder spring that pushes against the flyweights</span>
* <span style="font-size: large">Oil pressure into the hub increases the force on the piston and moves the blades towards a coarse pitch</span>
* <span style="font-size: large">Oil draining out of the hub decreases the force on the piston and moves the blades towards a fine pitch</span>

---

