## Engine Power/RPM { data-transition="convex-in slide-out" }

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/mppc/throttle-quadrant.jpg" alt="Throttle Quadrant" height="300px"/>
</p>

* At low RPM, the engine/propeller isn't designed to produce large amounts of HP &mdash; forced induction engines are especially vulnerable
* Promotes engine detonation
* Remember our propeller relies on RPM to resist blade bending forces!

---

